﻿using Application.Abstractions.Services.Catalog;
using Application.Wrapper;
using Shared.DTOs.Catalog;
using System;
using System.Threading.Tasks;

namespace Application.Services.Catalog
{
    public class ProductService : IProductService
    {
        public Task<Result<Guid>> CreateProductAsync(CreateProductRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<Result<Guid>> DeleteProductAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<Result<ProductDto>> GetByIdUsingDapperAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<Result<ProductDetailsDto>> GetProductDetailsAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<PaginatedResult<ProductDto>> GetProductsAsync(ProductListFilter filter)
        {
            throw new NotImplementedException();
        }

        public Task<Result<Guid>> UpdateProductAsync(UpdateProductRequest request, Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
