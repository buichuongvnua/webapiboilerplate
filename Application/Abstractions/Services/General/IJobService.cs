﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Application.Abstractions.Services.General
{
    public interface IJobService
    {
        string Enqueue(Expression<Func<Task>> methodCall);
    }
}
