﻿using Application.Abstractions.Services;
using System.Threading.Tasks;

namespace Application.Abstractions.Services.Identity
{
    public interface IPermissionService : ITransientService
    {
        public Task<bool> HasPermissionAsync(string userId, string permission);
    }
}
