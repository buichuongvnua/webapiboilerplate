﻿using Application.Abstractions.Services;
using Application.Wrapper;
using Shared.DTOs.Identity;
using Shared.DTOs.Identity.Requests;
using Shared.DTOs.Identity.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Abstractions.Services.Identity
{
    public interface IUserService : ITransientService
    {
        Task<Result<List<UserDetailsDto>>> GetAllAsync();

        Task<IResult<UserDetailsDto>> GetAsync(string userId);

        Task<IResult<UserRolesResponse>> GetRolesAsync(string userId);

        Task<IResult<string>> AssignRolesAsync(string userId, UserRolesRequest request);
    }
}
