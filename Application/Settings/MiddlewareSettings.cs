﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Settings
{
    public class MiddlewareSettings
    {
        public bool EnableRequestLogging { get; set; } = false;
        public bool EnableLocalization { get; set; } = false;
    }
}
