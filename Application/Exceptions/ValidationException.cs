﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Application.Exceptions
{
    public class ValidationException : CustomException
    {
        public ValidationException(List<string> errors = default) : base("Validation Failures Occured.", errors, HttpStatusCode.BadRequest)
        {
        }
    }
}
