﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
    public enum FileType
    {
        [Description(".jpg,.png,.jpeg")]
        Image
    }
}
