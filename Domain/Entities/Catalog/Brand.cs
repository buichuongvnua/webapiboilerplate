﻿using Domain.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Catalog
{
    public class Brand : AuditableEntity
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string TenantKey { get; set; }

        public Brand(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public Brand Update(string name, string description)
        {
            if (!Name.Equals(name)) Name = name;
            if (!Description.Equals(description)) Description = description;
            return this;
        }
    }
}
