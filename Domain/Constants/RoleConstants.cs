﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Constants
{
    public static class RoleConstants
    {
        public const string Admin = "Admin";
    }

    public static class ClaimConstants
    {
        public const string Permission = "Permission";
    }
}
