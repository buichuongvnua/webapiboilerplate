﻿using Application.Abstractions.Services.Catalog;
using Domain.Constants;
using Infrastructure.Identity.Permissions;
using Microsoft.AspNetCore.Mvc;
using Shared.DTOs.Catalog;
using System;
using System.Threading.Tasks;

namespace WebApi.Controllers.v1
{
    public class ProductsController : BaseController
    {
        private readonly IProductService _service;

        public ProductsController(IProductService service)
        {
            _service = service;
        }

        [HttpGet("{id}")]
        [MustHavePermission(Permissions.Products.View)]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var product = await _service.GetProductDetailsAsync(id);
            return Ok(product);
        }

        [HttpGet]
        [MustHavePermission(Permissions.Products.ListAll)]
        public async Task<IActionResult> GetListAsync(ProductListFilter filter)
        {
            var products = await _service.GetProductsAsync(filter);
            return Ok(products);
        }

        [HttpGet("dapper")]
        public async Task<IActionResult> GetDapperAsync(Guid id)
        {
            var products = await _service.GetByIdUsingDapperAsync(id);
            return Ok(products);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(CreateProductRequest request)
        {
            return Ok(await _service.CreateProductAsync(request));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync(UpdateProductRequest request, Guid id)
        {
            return Ok(await _service.UpdateProductAsync(request, id));
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var productId = await _service.DeleteProductAsync(id);
            return Ok(productId);
        }
    }
}
