﻿using Application.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Extensions
{
    public static class SettingExtensions
    {
        internal static IServiceCollection AddSettings(this IServiceCollection services, IConfiguration config)
        {
            services
                .Configure<MiddlewareSettings>(config.GetSection(nameof(MiddlewareSettings)))
                .Configure<CacheSettings>(config.GetSection(nameof(CacheSettings)))
                .Configure<SwaggerSettings>(config.GetSection(nameof(SwaggerSettings)));
            return services;
        }
    }
}
