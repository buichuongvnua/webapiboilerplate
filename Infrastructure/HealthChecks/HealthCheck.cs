﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.HealthChecks
{
    public class HealthCheck : IHealthCheck
    {
        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            var check = new HealthCheckResult(HealthStatus.Healthy);
            return await Task.FromResult<HealthCheckResult>(check);
        }
    }
}
