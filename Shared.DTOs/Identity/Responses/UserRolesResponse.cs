﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DTOs.Identity.Responses
{
    public class UserRolesResponse
    {
        public List<UserRoleDto> UserRoles { get; set; } = new();
    }
}
