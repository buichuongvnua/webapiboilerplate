﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DTOs.Filters
{
    public class BaseFilter
    {
        public Search Search { get; set; }
    }
}
