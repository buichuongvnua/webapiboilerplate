﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DTOs.Catalog
{
    public class CreateBrandRequest : IMustBeValid
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
